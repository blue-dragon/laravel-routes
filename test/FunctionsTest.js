const chai = require('chai');
const expect = chai.expect;
const sinonChai = require('sinon-chai');
const sinon = require('sinon');
const assert = chai.assert;
const LaravelRoutes = require('../resources/views/functions.blade.php');

chai.use(sinonChai);

describe('functions', function () {
    let server;

    before(() => {
        sinon.spy(console, 'info');
        sinon.spy(console, 'error');
        global.XMLHttpRequest = sinon.useFakeXMLHttpRequest();
        server = sinon.fakeServer.create();
        server.respondImmediately = true;
    });
    after(() => {
        server.restore();
    });

    it('can_construct_laravel_routes', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
    });

    it('can_construct_laravel_routes_with_json_object', function () {
        const jsonObject = '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}';
        new LaravelRoutes(jsonObject);
        expect(console.info).to.be.calledWith('Successfully loaded the routes');
    });

    it('can_not_construct_a_file_that_returns_400', function () {
        server.respondWith('GET', 'test.json', [
            400,
            { "Content-Type": "application/json" },
            '{"error": "error"}'
        ]);
        new LaravelRoutes('test.json');
        expect(console.error).to.be.calledWith('Failed to load the routes file: test.json');
    });

    it('can_not_construct_a_file_that_returns_500', function () {
        server.respondWith('GET', 'test.json', [
            0,
            {},
            ''
        ]);
        new LaravelRoutes('test.json');
        expect(console.error).to.be.calledWith('Failed to load the routes file: test.json');
    });

    it('can_get_an_existing_route_without_parameters', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        const laravelRoutes = new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
        assert.equal('/home', laravelRoutes.route('home'));
    });

    it('can_get_an_existing_route_with_null_as_parameters', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        const laravelRoutes = new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
        assert.equal('/home', laravelRoutes.route('home', null));
    });

    it('can_not_get_a_non_existing_route', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        const laravelRoutes = new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
        assert.isUndefined(laravelRoutes.route('test'));
        expect(console.info).to.be.calledWith('Route test not found!');
    });

    it('can_get_an_existing_route_with_parameters', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        const laravelRoutes = new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
        assert.equal('/paramtest/1', laravelRoutes.route('paramtest', {param1: '1'}));
    });

    it('can_get_an_existing_route_with_get_parameters', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        const laravelRoutes = new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
        assert.equal('/home?param1=1&param2=2', laravelRoutes.route('home', {param1: 1, param2: 2}));
    });

    it('can_not_get_a_route_with_missing_required_parameter', function () {
        server.respondWith('GET', 'test.json', [
            200,
            { "Content-Type": "application/json" },
            '{"routes":{"home":{"methods":["GET","HEAD"], "uri":"home"},"paramtest":{"methods":["GET","HEAD"],"uri":"paramtest/{param1}"}}}'
        ]);
        const laravelRoutes = new LaravelRoutes('test.json');
        expect(console.info).to.be.calledWith('Successfully loaded the routes file');
        laravelRoutes.route('paramtest');
        expect(console.error).to.be.calledWith('Missing parameter param1');
    });
});
