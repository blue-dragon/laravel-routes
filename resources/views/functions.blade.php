/**
 * Laravel routes
 * @author    Niek Vaanholt <n.vaanholt@bluedragon.nl>
 */

/**
 * Here we construct the new laravel routes instance
 *
 * @param  {string} routes
 */
const LaravelRoutes = function(routes) {
  let isValidJSON = true;

  try {
    routes = JSON.parse(routes);
  } catch (e) {
    isValidJSON = false;
  }

  if (isValidJSON) {
    LaravelRoutes.prototype.routes = routes.routes;
    console.info('Successfully loaded the routes');
  } else {
    const request = new XMLHttpRequest();
    request.open('GET', routes, true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        console.info('Successfully loaded the routes file');
        LaravelRoutes.prototype.routes = JSON.parse(request.responseText).routes;
      } else {
        console.error('Failed to load the routes file: ' + routes);
      }
    };

    request.onerror = function() {
      console.error('Failed to load the routes file: ' + routes);
    };

    request.send();
  }
};

/**
 * Here we build the uri
 *
 * @param  {string} uri
 * @param  {object} routeParameters
 *
 * @return  {string}
 */
LaravelRoutes.prototype.buildUrl = function(uri, routeParameters) {
  const parameters = LaravelRoutes.prototype.getParametersFromUri(uri);
  parameters.forEach(function(parameter) {
    if (routeParameters.hasOwnProperty(parameter.name)) {
      parameter.value = String(routeParameters[parameter.name]);
    }

    if (parameter.required && !parameter.value.length) {
      console.error('Missing parameter ' + parameter.name);
      return;
    }

    delete routeParameters[parameter.name];
    uri = uri.replace(parameter.fragment, parameter.value);
  });

  let firstParameter = true;
  Object.keys(routeParameters).forEach(function(name) {
    if (firstParameter) {
      uri += '?' + name + '=' + routeParameters[name];
      firstParameter = false;
      return;
    }
    uri += '&' + name + '=' + routeParameters[name];
  });
  return uri;
};

/**
 * Here we get the uri parameters
 *
 * @param  {string} uri
 *
 * @return  {array}
 */
LaravelRoutes.prototype.getParametersFromUri = function(uri) {
  const regex = new RegExp(/{(.*?)}/g);
  let match;
  const parameters = [];
  while (match = regex.exec(uri)) {
    parameters.push({
      fragment: match[0],
      name: match[1].replace('?', ''),
      required: match[0].indexOf('?') < 0,
      value: ''
    });
  }
  return parameters;
};

/**
 * Here we get the route
 *
 * @param  {string} routeName
 * @param  {object} routeParameters
 *
 * @return  {string}
 */
LaravelRoutes.prototype.route = function(routeName, routeParameters) {
  if (routeParameters === undefined || routeParameters === null) {
    routeParameters = {};
  }
  if (!LaravelRoutes.prototype.routes.hasOwnProperty(routeName)) {
    console.info('Route ' + routeName + ' not found!');
    return;
  }
  return '/' + LaravelRoutes.prototype.buildUrl(LaravelRoutes.prototype.routes[routeName].uri, routeParameters);
};

/* istanbul ignore if  */
if (typeof module === 'object' && module && typeof module.exports === 'object') {
  module.exports = LaravelRoutes;
}
