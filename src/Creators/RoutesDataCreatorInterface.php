<?php


namespace BlueDragon\LaravelRoutes\Creators;

use Illuminate\Support\Collection;

/**
 * Interface RoutesDataCreatorInterface
 */
interface RoutesDataCreatorInterface
{
    /**
     * Get the collection with the data from the routes for a group
     *
     * @param string $group
     *
     * @return Collection
     */
    public function getRoutesData(string $group) : Collection;
}
