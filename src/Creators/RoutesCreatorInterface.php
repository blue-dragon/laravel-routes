<?php


namespace BlueDragon\LaravelRoutes\Creators;

/**
 * Interface RoutesCreatorInterface
 */
interface RoutesCreatorInterface
{
    /**
     * Get the string with all the information about the routes from a group
     *
     * @param string $group
     *
     * @return string
     */
    public function getRoutesScript(string $group) : string;
}
