# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]


## 2.9.0 - 2023-02-25
### Added
- Add Laravel 10 support.

## 2.8.0 - 2022-12-12
### Added
- Add PHP 8.2 support.

## 2.7.0 - 2022-08-16
### Added
-Possibility to load JSON string with constructor

## 2.6.0 - 2022-02-8
### Fixed
- Fixed PHP version constraint in composer.json

### Added
- Add missing information in composer.json 
- Add Laravel 9 support.

## 2.5.0
### Added
- Added Laravel 8 support.

## 2.4.0
### Added
- Added Laravel 7 support.

## 2.3.0
- Use async XMLHttpRequest as default.
- Remove the route methods, feedback from @sergiz

## 2.2.0
- The PublishAllCommand now respects --no-interaction artisan calls, and does not show the progressbar on stderr.
- Added CodeSniffer dependency to composer dev requirements, as the contributing refers to it.

## 2.1.0
- Changed the javascript to support null for the route parameters. Thanks to @danny7 for the idea.

## 2.0.0

### Added
- Added Laravel 6.0 support.

### Changed
- Depend on the `Illuminate\Filesystem\Filesystem` and not the `Illuminate\Filesystem\FilesystemManager` in the publishers classes.
- Change the default directory to save the files to `public/laravel_route` from the root and not the storage.

### Removed
- Remove the disk option, (This was in fact a work around to publish the files to the public directory)

### Internal
- Change the GitLab CI configuration, split the different jobs to use custom images instead of using composer packages. And change the GitLab CI config.

## 1.1.1
- Changed the javascript class to prototype for IE11 support.
- Fixed Use the composer package and not the phar for phpmd, prevent problems because phpmd.org is down.

## 1.1.0
- Added option to change the disk used to store the files.
- Added test directory to .gitattributes to ignore it in the export.
- Fixed the function that builds the route in the functions.blade.php to return an absolute url instead of a relative url.
- Fixed multiple typos and logo url in README.

## 1.0.0
- Add the first version.
